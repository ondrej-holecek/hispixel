# hispixel

Terminal application based on GTK3.0 and VTE.

- minimalistic design
- highly configurable
- all decroations can be disabled
- thanks https://github.com/volca02/mypixels


Build instructions:

```
$ autoreconf -if
$ ./configure
$ make
```